from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow

#Aplicaciones Distribuidas.
#Integrantes: Byron Guaman, Kevin Ulcuango, Alexis Vaca, Juan Iza; 
#Fecha: 08/02/2022

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://admin:admin123@database-1.cub4bdthamoz.us-east-1.rds.amazonaws.com:3306/bddapp'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)
ma = Marshmallow(app)

# Clases y métodos para los usuarios:


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    fullname = db.Column(db.String(70), unique=True)
    passwd = db.Column(db.String(15))
    birthdate = db.Column(db.String(15))
    email = db.Column(db.String(50))
    cellphone = db.Column(db.String(15))
    founds = db.Column(db.Float)

    def __init__(self, fullname, passwd, birthdate, email, cellphone, founds):
        self.fullname = fullname
        self.passwd = passwd
        self.birthdate = birthdate
        self.email = email
        self.cellphone = cellphone
        self.founds = founds


db.create_all()


class userSchema(ma.Schema):
    class Meta:
        fields = ('id', 'fullname', 'passwd', 'birthdate',
                  'email', 'cellphone', 'founds')


user_schema = userSchema()
users_schema = userSchema(many=True)


@app.route('/users', methods=['Post'])
def create_user():

    fullname = request.json['fullname']
    passwd = request.json['passwd']
    birthdate = request.json['birthdate']
    email = request.json['email']
    cellphone = request.json['cellphone']
    founds = request.json['founds']

    new_user = User(fullname, passwd, birthdate, email, cellphone, founds)

    db.session.add(new_user)
    db.session.commit()

    return user_schema.jsonify(new_user)


@app.route('/users', methods=['GET'])
def get_users():
    all_users = User.query.all()
    result = users_schema.dump(all_users)
    return jsonify(result)


@app.route('/users/<id>', methods=['GET'])
def get_user(id):
    user = User.query.get(id)
    return user_schema.jsonify(user)


@app.route('/users/<id>', methods=['PUT'])
def update_user(id):
    user = User.query.get(id)

    fullname = request.json['fullname']
    passwd = request.json['passwd']
    birthdate = request.json['birthdate']
    email = request.json['email']
    cellphone = request.json['cellphone']
    founds = request.json['founds']

    user.fullname = fullname
    user.passwd = passwd
    user.birthdate = birthdate
    user.email = email
    user.cellphone = cellphone
    user.founds = founds

    db.session.commit()

    return user_schema.jsonify(user)


@app.route('/users/<id>', methods=['DELETE'])
def delete_user(id):
    user = User.query.get(id)
    db.session.delete(user)
    db.session.commit()
    return user_schema.jsonify(user)


# Clases y métodos para eventos
class Event(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    code = db.Column(db.String(10), unique=True)
    description = db.Column(db.String(100))
    sport = db.Column(db.String(50))
    date = db.Column(db.String(15))
    team1 = db.Column(db.String(50))
    team2 = db.Column(db.String(50))
    winner = db.Column(db.String(50))

    def __init__(self, code, description, sport, date, team1, team2, winner):
        self.code = code
        self.description = description
        self.sport = sport
        self.date = date
        self.team1 = team1
        self.team2 = team2
        self.winner = winner


db.create_all()


class EventSchema(ma.Schema):
    class Meta:
        fields = ('id', 'code', 'description', 'sport',
                  'date', 'team1', 'team2', 'winner')


event_schema = EventSchema()
events_schema = EventSchema(many=True)


@app.route('/events', methods=['Post'])
def create_event():

    code = request.json['code']
    description = request.json['description']
    sport = request.json['sport']
    date = request.json['date']
    team1 = request.json['team1']
    team2 = request.json['team2']
    winner = request.json['winner']
    new_event = Event(code, description, sport, date, team1, team2, winner)

    db.session.add(new_event)
    db.session.commit()

    return event_schema.jsonify(new_event)


@app.route('/events', methods=['GET'])
def get_events():
    all_events = Event.query.all()
    result = events_schema.dump(all_events)
    return jsonify(result)


@app.route('/events/<id>', methods=['GET'])
def get_event(id):
    event = Event.query.get(id)
    return event_schema.jsonify(event)


@app.route('/events/<id>', methods=['DELETE'])
def delete_event(id):
    event = Event.query.get(id)
    db.session.delete(event)
    db.session.commit()
    return event_schema.jsonify(event)


@app.route('/events/<id>', methods=['PUT'])
def update_event(id):
    event = Event.query.get(id)

    code = request.json['code']
    description = request.json['description']
    sport = request.json['sport']
    date = request.json['date']
    team1 = request.json['team1']
    team2 = request.json['team2']
    winner = request.json['winner']

    event.code = code
    event.description = description
    event.sport = sport
    event.date = date
    event.team1 = team1
    event.team2 = team2
    event.winner = winner

    db.session.commit()
    return event_schema.jsonify(event)


@app.route('/', methods=['GET'])
def index():
    return jsonify({'message': 'API REST ONLINE: Byron Guaman, Kevin Ulcuango, Alexis Vaca, Juan Iza '})


if __name__ == "__main__":
    app.run()
